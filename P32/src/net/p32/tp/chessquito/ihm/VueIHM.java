package net.p32.tp.chessquito.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import net.p32.tp.chessquito.ControleurChessquito;

public class VueIHM extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Color NOIR = new Color(83, 83, 83);
	public static final Color BLANC = new Color(240, 240, 240);
	
	private JTextArea messages;
	private JPanel panneauPlateau;

	public VueIHM() {
		ControleurChessquito controleur = new ControleurChessquito(this);

		this.setLayout(new BorderLayout());
		this.panneauPlateau = new JPanel(new GridLayout(4, 4));
		for(int i = 0; i < 16; i++) {
			Color c = BLANC;
			if(i % 2 == (i/4 % 2))c = NOIR;
			CaseIHM caze = new CaseIHM(i/4, i%4, c);
			caze.addActionListener(controleur);
			this.panneauPlateau.add(caze);
		}
		JPanel panneauMsg = new JPanel();
		this.messages = new JTextArea(4, 40);
		this.messages.setEditable(false);
		panneauMsg.add(new JScrollPane(this.messages));
		controleur.rafraichir();
		this.panneauPlateau.setVisible(true);
		
		this.add(this.panneauPlateau, BorderLayout.CENTER);
		this.add(panneauMsg, BorderLayout.SOUTH);
	}

	public JTextArea getMessages() {
		return messages;
	}
	
	public JPanel getPanneauPlateau() {
		return panneauPlateau;
	}
	
	public void afficherMessage(String message) {
		this.messages.setText("");
		this.messages.append(message);
	}

	public void positionnerPiece(PieceIHM piece, int ligne, int colomne) {
		JButton bouton = (JButton) this.panneauPlateau.getComponent(ligne*4+colomne);
		bouton.setIcon(piece);
	}
}
