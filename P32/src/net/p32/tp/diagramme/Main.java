package net.p32.tp.diagramme;

import java.io.IOException;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		VueJeton vueJeton;
		try {
			vueJeton = new VueJeton();
			JFrame jFrame = new JFrame();
			jFrame.add(vueJeton);
			jFrame.setSize(600, 600);
			jFrame.setVisible(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}

