package net.p32.tp.chessquito;

import javax.swing.JFrame;

import net.p32.tp.chessquito.ihm.PieceIHM;
import net.p32.tp.chessquito.ihm.VueIHM;

public class FenetreChess {

	public static void main(String[] args) {
		VueIHM vue = new VueIHM();
		
		JFrame jframe = new JFrame();
		jframe.setSize(600, 600);
		jframe.setTitle("Chessquito");
		jframe.add(vue);
		jframe.setResizable(false);
		jframe.setVisible(true);
	}
	 
}
