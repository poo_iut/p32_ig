package net.p32.tp.chessquito.ihm;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import net.p32.tp.chessquito.FenetreChess;

public class PieceIHM extends ImageIcon{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7570350608393481908L;

	public static final String COULEUR_PIECE_NOIR = "Noir";
	public static final String COULEUR_PIECE_BLANC = "Blanc";

	
	private String couleurPiece;
	private String nomPiece;
	private String typeImage;
	
	public PieceIHM(String couleurPiece, String nomPiece, String typeImage) {
		this.couleurPiece = couleurPiece;
		this.nomPiece = nomPiece;
		this.typeImage = typeImage;
		try {
			String image = nomPiece+couleurPiece+typeImage+".gif";
			setImage(
					ImageIO.read(FenetreChess.class.getResourceAsStream("./images/"+image)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	

	@Override
	public String toString() {
		return "PieceIHM ["+couleurPiece + " " + nomPiece + " " + typeImage + "]";
	}



	public String getCouleurPiece() {
		return couleurPiece;
	}

	public String getNomPiece() {
		return nomPiece;
	}

	public String getTypeImage() {
		return typeImage;
	}
	
	
	
	
	
}
