package net.p32.tp.mastermind;

public class ModeleMastermind {

    /**
     * combinaison ordinateur
     */
    private int combinaison[];

    /**
     * nombre de valeurs entieres differentes pouvant etre generees
     */
    private static final int NB_VALEURS = 6;

    /**
     * taille de la combinaison
     */
    private static final int TAILLE = 4;

    private int nbValeurs;
    
    /**
     * cree une instance ModeleMastermind
     */
    public ModeleMastermind() {
    	this(TAILLE, NB_VALEURS);
    }
    
    /**
     * cree une instance ModeleMastermind
     */
    public ModeleMastermind(int taille, int nbValeurs) {
        this.combinaison = new int[taille];
        this.nbValeurs = nbValeurs;
        genererCombinaison();
    }

    /**
     * genere aleatoirement une combinaison de taille taille dont les valeurs
     * sont comprises entre 0 et nbValeurs-1
     */
    public void genererCombinaison() {
    	System.out.println("Taille combi ");
        for (int i = 0; i < this.combinaison.length; i++) {
            this.combinaison[i] = (int) (this.nbValeurs * Math.random());
        }
    }

    /**
     * renvoie la combinaison de l'ordinateur
     * 
     * @return tableau representant la combinaison
     */
    public int[] getCombinaison() {
        return (this.combinaison);
    }

    /**
     * indique le nombre de chiffres bien places dans le tableau passe en
     * parametre
     * 
     * @param tableau
     *            contenant la combinaison a verifier
     * @return nombre de chiffres bien places
     */
    public int nbChiffresBienPlaces(int tabChiffres[]) {
        int v = 0;
        for (int i = 0; i < this.combinaison.length; i++) {
            if (this.combinaison[i] == tabChiffres[i]) {
                v++;
            }
        }
        return v;
    }
    
    public boolean estCorrect(int[] tabChiffre) {
    	return this.nbChiffresBienPlaces(tabChiffre) == this.combinaison.length;
    }

    /**
     * indique le nombre de chiffres mal places dans le tableau passe en
     * parametre
     * 
     * @param tableau
     *            contenant la combinaison a verifier
     * @return nombre de chiffres mal places
     */
    public int nbChiffresMalPlaces(int tabChiffres[]) {
        int v = 0;
        int combinaisonAux[] = new int[this.combinaison.length];
        int tabChiffresAux[] = new int[this.combinaison.length];
        for (int i = 0; i < this.combinaison.length; i++) {
            combinaisonAux[i] = combinaison[i];
            tabChiffresAux[i] = tabChiffres[i];
            if (tabChiffresAux[i] == combinaisonAux[i]) {
                combinaisonAux[i] = -1;
                tabChiffresAux[i] = -2;
            }
        }
        for (int i = 0; i < this.combinaison.length; i++) {
            boolean trouve = false;
            for (int j = 0; j < this.combinaison.length && !trouve; j++) {
                if (tabChiffresAux[i] == combinaisonAux[j]) {
                    v++;
                    combinaisonAux[j] = -1;
                    tabChiffresAux[i] = -2;
                    trouve = true;
                }
            }
        }
        return v;
    }

    /**
     * produit une version unicode de la combinaison
     * 
     * @return la combinaison
     */
    public String toString() {
        String c = "( ";
        for (int i = 0; i < this.combinaison.length; i++) {
            c = c + this.combinaison[i] + " ";
        }
        c = c + ")";
        return c;
    }
}
