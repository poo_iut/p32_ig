package net.p32.tp.mastermind;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import net.p32.tp.mastermind.controleurs.ControleurRejouer;
import net.p32.tp.mastermind.controleurs.ControleurRestaurer;
import net.p32.tp.mastermind.controleurs.ControleurSauvegarder;
import net.p32.tp.mastermind.controleurs.options.ControleurCombinaison;
import net.p32.tp.mastermind.controleurs.options.ControleurCouleur;

public class FenetreMastermind extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7801164949378322672L;
	private VueMasterMind vue;
	private JMenuItem nbCouleur;
	private JMenuItem nbTailleCombinaison;
	
	public FenetreMastermind() {
		this.setSize(1280, 720);
		int nbCouleur = 5;
		int tailleCombinaison = 4;
		this.vue = new VueMasterMind(tailleCombinaison, 10, nbCouleur);
		
		JMenuBar menu = new JMenuBar();
		
		JMenu jeu = new JMenu("Jeu");
		JMenuItem sauvegarder = new JMenuItem("Sauvegarder");
		sauvegarder.addActionListener(new ControleurSauvegarder(this));
		JMenuItem rejouer = new JMenuItem("Rejouer");
		rejouer.addActionListener(new ControleurRejouer(this));
		JMenuItem restaurer = new JMenuItem("Restaurer");
		restaurer.addActionListener(new ControleurRestaurer(this));
		JMenuItem quitter = new JMenuItem("Quitter");
		jeu.add(sauvegarder);
		jeu.add(rejouer);
		jeu.add(restaurer);
		jeu.addSeparator();
		jeu.add(quitter);
		
		JMenu options = new JMenu("Options");
	
		// Nombre couleurs
		JMenu nbCouleurs = new JMenu("Nombre de Couleurs");
		ControleurCouleur controleurCouleur = new ControleurCouleur(this);
		for(int i = 2; i <= VueMasterMind.COLORS.length; i++) {
			JMenuItem jItem = new JMenuItem(String.valueOf(i));
			jItem.addActionListener(controleurCouleur);
			if(i == nbCouleur) {
				this.nbCouleur = jItem;
				jItem.setBackground(Color.GREEN);
			}
			nbCouleurs.add(jItem);
		}
		
		// Taille de la combinaison
		JMenu tailleCombi = new JMenu("Taille combinaison");
		ControleurCombinaison controleurCombinaison = new ControleurCombinaison(this);
		for(int i = 1; i <= VueMasterMind.COLORS.length; i++) {
			JMenuItem jItem = new JMenuItem(String.valueOf(i));
			jItem.addActionListener(controleurCombinaison);
			if(i == tailleCombinaison) {
				this.nbTailleCombinaison = jItem;
				jItem.setBackground(Color.GREEN);
			}
			tailleCombi.add(jItem);
		}
		
		
		options.add(nbCouleurs);
		options.add(tailleCombi);
		menu.add(jeu);
		menu.add(options);
		this.setJMenuBar(menu);
		this.add(this.vue);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public void creerNouvelleVue() {
		int nbCouleur = Integer.parseInt(this.nbCouleur.getText());
		int tailleCombinaison = Integer.parseInt(this.nbTailleCombinaison.getText());
		this.creerNouvelleVue(new VueMasterMind(tailleCombinaison, 10, nbCouleur));
	}
	
	public void creerNouvelleVue(VueMasterMind vue) {
		System.out.println("[DEBUG] Création d'une nouvelle vue : "+vue.getNbCouleur()+" / "+vue.getNbChoix());
		this.remove(this.vue);
		this.add(vue);
		this.setVisible(true);
		this.repaint();
		System.out.println("[DEBUG] Page repainte");
		this.vue = vue;
	}
	
	public void changerItemNbCouleurs(JMenuItem jItem) {
		this.nbCouleur.setBackground(null);
		this.nbCouleur = jItem;
		this.nbCouleur.setBackground(Color.GREEN);
	}
	
	public void changerItemTailleCombi(JMenuItem jItem) {
		this.nbTailleCombinaison.setBackground(null);
		this.nbTailleCombinaison = jItem;
		this.nbTailleCombinaison.setBackground(Color.GREEN);
	}
	
	public String dateActuelle() {
		DateFormat format = new SimpleDateFormat("dd-M-yyyy_hh:mm:ss");
		return format.format(new Date());
	}
	
	public void restaurerVueMasterMind(String nomFichier) {
		File file = new File("saves/"+nomFichier);
		try {
			FileInputStream in = new FileInputStream(file);
			ObjectInputStream objIn = new ObjectInputStream(in);
			VueMasterMind vue = (VueMasterMind) objIn.readObject();
			this.creerNouvelleVue(vue);
			objIn.close();
		}catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public String getFileExtension() {
		return ".mastermind";
	}
	
	public void sauvegarderVueMasterMind(String nomFichier) {
		File file = new File("saves/"+nomFichier);
		try {
			FileOutputStream out = new FileOutputStream(file);
			ObjectOutputStream objOut = new ObjectOutputStream(out);
			objOut.writeObject(this.vue);
			objOut.flush();
			objOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
