package net.p32.tp.diagramme;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ControleurJeton implements ActionListener {

	public static enum Clic {
		ATTENTE, ARRIVE;
	}

	private Clic etat;
	private JButton actuelClic;

	public ControleurJeton() {
		this.etat = Clic.ATTENTE;
	}

	public Clic getÉtat() {
		return etat;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (this.etat) {
		case ARRIVE:
			if(VueJeton.estCaseVide((JButton) e.getSource())) {
				VueJeton.deplacerJeton(this.actuelClic, (JButton) e.getSource());
				this.etat = Clic.ATTENTE;
				this.actuelClic = null;
			}
			break;
		case ATTENTE:
			if(!VueJeton.estCaseVide((JButton) e.getSource())) {
				this.actuelClic = (JButton) e.getSource();
				this.etat = Clic.ARRIVE;
			}
			break;
		
		}
	}

}
