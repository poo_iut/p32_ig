package net.p32.tp.chessquito;

import static org.junit.Assert.assertEquals;

import javax.swing.JButton;

import org.junit.Test;

import net.p32.tp.chessquito.ihm.PieceIHM;
import net.p32.tp.chessquito.ihm.VueIHM;

public class TestChess {

	private VueIHM vue;
	
	public TestChess() {
		this.vue = new VueIHM();
	}
	
	@Test
	public void testMsgEcrit() {
		String messageEcrit = "Le Message";
		this.vue.afficherMessage(messageEcrit);
		assertEquals("Le Message", this.vue.getMessages().getText());
	}
	
	@Test
	public void testPiece() {
		PieceIHM piece = new PieceIHM("Noir", "cavalier", "Fixe");
		this.vue.positionnerPiece(piece, 0, 0);
		JButton boutton = (JButton) this.vue.getPanneauPlateau().getComponent(0);
		assertEquals(piece, boutton.getIcon());
	}
	
}
