package net.p32.tp.mastermind.controleurs.options;

import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;

import net.p32.tp.mastermind.FenetreMastermind;
import net.p32.tp.mastermind.controleurs.ControleurFenetre;

public class ControleurCombinaison extends ControleurFenetre{

	public ControleurCombinaison(FenetreMastermind instance) {
		super(instance);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JMenuItem numCombi = (JMenuItem) e.getSource();
		getInstance().changerItemTailleCombi(numCombi);
	}

	

}
