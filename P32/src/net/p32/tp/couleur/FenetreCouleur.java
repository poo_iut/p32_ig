package net.p32.tp.couleur;

import java.awt.GridLayout;

import javax.swing.JFrame;

/**
 * Cette classe permet de construire une <b>fenetre</b> composee d'un
 * PanneauCouleur.
 * 
 * 
 * @author Christine JULIEN
 * @version 1.1.6
 * @see java.awt.JFrame
 * @see java.awt.Canvas
 * @see java.awt.GridLayout
 * @see java.awt.event.WindowAdapter
 * @see java.awt.event.WindowEvent
 * @see PanneauCouleur
 */

public class FenetreCouleur extends JFrame {
    /**
     * Construit une FenetreCouleur contenant un PanneauCouleur
     */
    public FenetreCouleur() {
        // associer un titre � la FenetreCouleur
        this.setTitle("Conversions RVB");
        // selectionner le gestionnaire de mise en page de la FenetreCouleur
        this.setLayout(new GridLayout(1, 1));
        // creer une vue couleur et l'ajouter a la FenetreCouleur
        this.add(new VueCouleur());
        // associer une taille � la FenetreCouleur
        this.setSize(250, 120);
        // afficher la FenetreCouleur
        this.pack();
        this.setVisible(true);
    }
}
