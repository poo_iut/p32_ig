package net.p32.tp.mastermind.controleurs.options;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import net.p32.tp.mastermind.FenetreMastermind;
import net.p32.tp.mastermind.controleurs.ControleurFenetre;

public class ControleurCouleur extends ControleurFenetre{

	public ControleurCouleur(FenetreMastermind instance) {
		super(instance);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JMenuItem numCouleur = (JMenuItem) e.getSource();
		getInstance().changerItemNbCouleurs(numCouleur);
	}

	

}
