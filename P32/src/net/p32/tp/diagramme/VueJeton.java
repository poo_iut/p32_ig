package net.p32.tp.diagramme;

import java.awt.GridLayout;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class VueJeton extends JPanel{

	private static ImageIcon jeton;
	
	static {
		try {
			jeton = new ImageIcon(ImageIO.read(new FileInputStream("resources/image.jpg")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static boolean estCaseVide(JButton caze) {
		return caze.getIcon() == null;
	}
	
	public static void deplacerJeton(JButton from, JButton to) {
		from.setIcon(null);
		to.setIcon(jeton);
	}
	
	
	public VueJeton() throws IOException{
		super.setLayout(new GridLayout(2,2));
		ControleurJeton controleur = new ControleurJeton();
		for(int i = 0; i < 4; i++) {
			JButton button = new JButton();
			if(i == 0)button.setIcon(jeton);
			button.addActionListener(controleur);
			this.add(button);
		}
	}
	
	

} 
