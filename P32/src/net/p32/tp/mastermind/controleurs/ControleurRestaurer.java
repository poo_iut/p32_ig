package net.p32.tp.mastermind.controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import net.p32.tp.mastermind.FenetreMastermind;

public class ControleurRestaurer extends ControleurFenetre {

	public ControleurRestaurer(FenetreMastermind instance) {
		super(instance);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		int res = JOptionPane.showConfirmDialog(null, "Voulez vous restaurez ?");
		switch(res) {
		case JOptionPane.NO_OPTION:
			break;
		case JOptionPane.YES_OPTION:
			String fileName =
				JOptionPane.showInputDialog("Rentrez le nom du fichier (sans l'extension)") +
				getInstance().getFileExtension();
			
			getInstance().restaurerVueMasterMind(fileName);

			JOptionPane.showConfirmDialog(null, "Fichier restauré");
			
			break;
		case JOptionPane.CANCEL_OPTION:
			break;
		}
		
	}


}
