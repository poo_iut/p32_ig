package net.p32.tp.morpion.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import net.p32.tp.morpion.model.ModeleMorpion;
import net.p32.utils.Pair;

public class VueMorpion extends JPanel {

	private JLabel texteVictoire;
	private JLabel joueurActuel;
	private JButton reset;
	private JButton[] cases;

	public VueMorpion() {
		this.setLayout(new BorderLayout());
		if (false) {
			try {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e) {
				e.printStackTrace();
			}
		}
		this.cases = new JButton[ModeleMorpion.TAILLE_TABLEAU];

		JPanel grille = new JPanel(new GridLayout(ModeleMorpion.TAILLE_LIGNE, ModeleMorpion.TAILLE_LIGNE));
		for (int i = 0; i < ModeleMorpion.TAILLE_TABLEAU; i++) {
			JButton jb = new JButton(" ");
			this.cases[i] = jb;
			grille.add(jb);
		}

		JPanel informations = new JPanel(new GridLayout(1, 2));

		this.reset = new JButton("effacer");
		this.joueurActuel = new JLabel("joueur X");
		informations.add(this.reset);
		informations.add(this.joueurActuel);

		this.texteVictoire = new JLabel("vid");

		this.add(informations, BorderLayout.NORTH);
		this.add(grille, BorderLayout.CENTER);
		this.add(this.texteVictoire, BorderLayout.SOUTH);

	}

	public Pair<Integer, Integer> coordonéesBtCaseGrille(JButton jb) {
		for(int i = 0; i < ModeleMorpion.TAILLE_TABLEAU; i++) {
			if(this.cases[i] == jb) {
				return new Pair<>(i / ModeleMorpion.TAILLE_LIGNE, i % ModeleMorpion.TAILLE_LIGNE);
			}
		}
		return new Pair<>(0,0);
	}
	
	public void initialiser() {
		afficherJoueurCourrant(1);
		for (JButton jButton : this.cases) {
			jButton.setText("");
			jButton.setEnabled(true);
		}
		this.texteVictoire.setText(" ");
	}

	public void afficherJoueurCourrant(int joueur) {
		this.joueurActuel.setText("Joueur " + joueur);
	}

	public void afficherGagnant(int joueur) {
		this.texteVictoire.setText("Le joueur " + joueur + " à gagné");
	}

	public JButton[] getCases() {
		return cases;
	}

	public JButton getReset() {
		return reset;
	}

}
