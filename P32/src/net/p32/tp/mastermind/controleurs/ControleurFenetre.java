package net.p32.tp.mastermind.controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import net.p32.tp.mastermind.FenetreMastermind;

public abstract class ControleurFenetre implements ActionListener{

	private FenetreMastermind instance;
	
	public ControleurFenetre(FenetreMastermind instance) {
		super();
		this.instance = instance;
	}

	public FenetreMastermind getInstance() {
		return instance;
	}
	
	@Override
	public abstract void actionPerformed(ActionEvent event) ;
	
}
