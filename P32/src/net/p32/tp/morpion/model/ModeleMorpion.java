package net.p32.tp.morpion.model;

import java.util.Arrays;

public class ModeleMorpion {

	public static final int TAILLE_LIGNE = 3;
	public static final int TAILLE_TABLEAU = TAILLE_LIGNE * TAILLE_LIGNE;
	
	public enum TypeCase{
		JOUEUR1("J1", "X"), JOUEUR2("J2", "O"), VIDE("VV", " s");
		
		String desc;
		String tag;
		
		TypeCase(String desc, String tag){
			this.desc = desc;
			this.tag = tag;
		}
		
		public String getTag() {
			return tag;
		}
	}
	
	private TypeCase[][] tableau; 
 	
	public ModeleMorpion() {
		this.tableau = new TypeCase[TAILLE_LIGNE][TAILLE_LIGNE];
		reinit();
	}
	
	public TypeCase[][] getTableau() {
		return tableau;
	}

	public boolean casesAlignées(int l, int c) throws IllegalArgumentException {
		if (l < 0 || l > 3 || c < 0 || c > 3) {
			throw new IllegalArgumentException("indice incorrect");
		}
		TypeCase t = this.tableau[l][c];
		if (this.tableau[l][c] == TypeCase.VIDE) {
			throw new IllegalArgumentException("case inoccupée");
		}
		boolean aligne = (this.tableau[0][c] == t && this.tableau[1][c] == t && this.tableau[2][c] == t);
		if (aligne) {
			return aligne;
		}
		aligne = (this.tableau[l][0] == t && this.tableau[l][1] == t && this.tableau[l][2] == t);
		if (aligne) {
			return aligne;
		}
		if ((l == 0 && c == 0) || (l == 1 && c == 1) || (l == 2 && c == 2)) {
			aligne = (this.tableau[0][0] == t && this.tableau[1][1] == t && this.tableau[2][2] == t);
		}
		if (aligne) {
			return aligne;
		}
		if ((l == 0 && c == 2) || (l == 1 && c == 1) || (l == 2 && c == 0)) {
			aligne = (this.tableau[0][2] == t && this.tableau[1][1] == t && this.tableau[2][0] == t);
		}
		return aligne;
	}
	
	public TypeCase getValeurCase(int x, int y) {
		if(x < 0 || x > this.tableau.length ||
		   y < 0 || y > this.tableau.length) {
			throw new IllegalArgumentException("Erreur, en dehors du tableau");
		}
		return this.tableau[x][y];
	}
	
	public void setValeurCase(int x, int y, TypeCase typeCase) {
		if(x < 0 || x > this.tableau.length ||
		   y < 0 || y > this.tableau.length) {
			throw new IllegalArgumentException("Erreur, en dehors du tableau");
		}
		this.tableau[x][y] = typeCase;
	}
	
	
	public void reinit() {
		for(int i = 0; i < this.tableau.length; i++) {
			Arrays.fill(this.tableau[i], TypeCase.VIDE);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for(int i = 0; i < this.tableau.length; i++) {
			for(int j = 0; j < this.tableau.length; j++) {
				stringBuilder.append(this.getValeurCase(i, j).desc+" ");
			}
			stringBuilder.append('\n');
		}
		return stringBuilder.toString();
	}
	
}
