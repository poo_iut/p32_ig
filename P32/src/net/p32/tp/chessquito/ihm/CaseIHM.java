package net.p32.tp.chessquito.ihm;

import java.awt.Color;

import javax.swing.JButton;

public class CaseIHM extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4421519697353179490L;

	private int colonne;
	private int ligne;

	public CaseIHM(int ligne, int colonne, Color couleur) {
		this.colonne = colonne;
		this.ligne = ligne;
		setBackground(couleur);
	}
	
	

	@Override
	public String toString() {
		return "CaseIHM [colonne=" + colonne + ", ligne=" + ligne + "]";
	}

	public int getColonne() {
		return colonne;
	}
	
	public int getLigne() {
		return ligne;
	}
	
}
