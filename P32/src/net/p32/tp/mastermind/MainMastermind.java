package net.p32.tp.mastermind;

import java.util.Scanner;

public class MainMastermind {

	public static void main(String[] args) {
		new FenetreMastermind();
	}
	
	
	public static void main2() {
		ModeleMastermind master = new ModeleMastermind();
		master.genererCombinaison();
		System.out.println(master);
		
		Scanner scanner = new Scanner(System.in);
		String ligne = "";
		System.out.println("Veuillez saisir une liste de chiffre bien placés");
		System.out.println("séparés par des ',' (Exemple: \"1,2,5,7\" ");
		System.out.println("Entrez \"exit\" pour sortir");
		ligne = scanner.nextLine();
		while(!ligne.equals("exit")) {
			String[] nombre = ligne.replaceAll("\\s+","").split("\\,");
			int[] n = new int[nombre.length];
			for (int i = 0; i < nombre.length; i++) {
				n[i] = Integer.parseInt(nombre[i]);
			}
			System.out.println("Nombre de chiffre bien placé : "+master.nbChiffresBienPlaces(n));
			System.out.println("Nombre de chiffre mal placé : "+master.nbChiffresMalPlaces(n));
			System.out.println("=========================================");
			System.out.println("Veuillez saisir une liste de chiffre bien placés");
			System.out.println("séparés par des ',' (Exemple: \"1,2,5,7\" ");
			System.out.println("Entrez \"exit\" pour sortir");
			ligne = scanner.nextLine();
		}
		scanner.close();
		return;
	}
	

}
