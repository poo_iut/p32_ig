package net.p32.tp.morpion.controllers;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import net.p32.tp.morpion.model.ModeleMorpion;
import net.p32.tp.morpion.model.ModeleMorpion.TypeCase;
import net.p32.tp.morpion.view.VueMorpion;
import net.p32.utils.Pair;

public class ControleurMorpion {

	public static void main(String[] args) {
		new ControleurMorpion();
	}

	private VueMorpion vueMorpion;
	private ModeleMorpion modeleMorpion;
	private TypeCase joueurActuel;
	
	public ControleurMorpion() {
		this.vueMorpion = new VueMorpion();
		this.modeleMorpion = new ModeleMorpion();
		this.joueurActuel = TypeCase.JOUEUR1;

		this.vueMorpion.initialiser();
		
		ActionListener resetListener = event -> {
			this.vueMorpion.initialiser();
			this.modeleMorpion.reinit();
		};
		
		ActionListener bouttonListener = event -> {
			JButton boutton = (JButton) event.getSource();
			boutton.setText(this.joueurActuel.getTag());
			Pair<Integer, Integer> p = this.vueMorpion.coordonéesBtCaseGrille(boutton);
			this.modeleMorpion.setValeurCase(p.getA(), p.getB(), this.joueurActuel);
			try {
				if(this.modeleMorpion.casesAlignées(p.getA(), p.getB())) {
					this.vueMorpion.afficherGagnant(this.joueurActuel.ordinal()+1);
					for(JButton jButton : this.vueMorpion.getCases()) {
						jButton.setEnabled(false);
					}
					return;
				}
			} catch (Exception e) {}
			boutton.setEnabled(false);
			this.joueurActuel = this.joueurActuel == TypeCase.JOUEUR1 ? TypeCase.JOUEUR2 : TypeCase.JOUEUR1;
			this.vueMorpion.afficherJoueurCourrant(this.joueurActuel.ordinal()+1);
		};
		
		
		for(JButton jButton : this.vueMorpion.getCases()) {
			jButton.addActionListener(bouttonListener);
		}
		
		this.vueMorpion.getReset().addActionListener(resetListener);
		
		JFrame jframe = new JFrame();
		jframe.setLayout(new GridLayout(1, 1));
		jframe.setSize(600, 600);
		jframe.add(this.vueMorpion, BorderLayout.CENTER);
		jframe.setResizable(false);
		jframe.setVisible(true);

	}

}
