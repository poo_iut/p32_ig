package net.p32.tp.mastermind.controleurs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JComponent;

import net.p32.tp.mastermind.ModeleMastermind;
import net.p32.tp.mastermind.VueMasterMind;

public class ControleurMastermind implements ActionListener{

	private enum Etat{
		DEBUT_COMBINAISON,
		CHOIX_COULEUR,
		CHOIX_POSITION;
	}
	
	private Etat etat;
	private VueMasterMind vue;
	private Color couleurSelect;
	private ModeleMastermind modele;
	private int combinaisonActuelle;
	private boolean fin;
	
	public ControleurMastermind(VueMasterMind vue) {
		this.etat = Etat.DEBUT_COMBINAISON;
		this.vue = vue;
		
	}

	private int nbEssaieRestant(int combinaisonActuelle) {
		return this.vue.getNbTours() - (this.combinaisonActuelle+1);
	}
	
	public void init() {
		// trait1
		this.modele = new ModeleMastermind(vue.getNbChoix(), vue.getNbCouleur());
		this.combinaisonActuelle = 0;
		this.vue.activerCombinaison(this.combinaisonActuelle);
		this.modele.genererCombinaison();
		this.fin = false;
		System.out.println("ModelCombi : "+Arrays.toString(this.modele.getCombinaison()));
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(this.fin)return;
		JComponent comp = (JComponent) event.getSource();
		switch (this.etat) {
		case CHOIX_COULEUR:
			if(comp instanceof JButton) {
				JButton boutton = (JButton) comp;
				// clic BtCombinaison / trait3
				if(this.vue.appartientCombinaison(boutton, this.combinaisonActuelle)) {
					boutton.setBackground(this.couleurSelect);
					this.couleurSelect = null;
					this.etat = Etat.CHOIX_POSITION;
				}
			}
			break;
		case CHOIX_POSITION:
			if(comp instanceof JButton) {
				JButton boutton = (JButton) comp;
				// clic BtCouleur / trait2
				if(this.vue.appartientPalette(boutton)) {
					this.etat = Etat.CHOIX_COULEUR;
					this.couleurSelect = comp.getBackground();
					break;
				}
				if(boutton.getText().equals("Valider")) {
					int[] valeurs = this.vue.combinaisonEnEntiers(this.combinaisonActuelle);
					int nbBienPlacé = this.modele.nbChiffresBienPlaces(valeurs);
					int nbMalPlacé = this.modele.nbChiffresMalPlaces(valeurs);
					System.out.println("NbEssai : "+this.nbEssaieRestant(this.combinaisonActuelle));
					System.out.println("Valeurs: "+Arrays.toString(valeurs));
					System.out.println("Valeurs ordi: "+Arrays.toString(this.modele.getCombinaison()));
					if(this.vue.combinaisonComplete(this.combinaisonActuelle)) {
						if(nbEssaieRestant(this.combinaisonActuelle) > 0
						&& !this.modele.estCorrect(valeurs)) {
							// trait4, trait5
							System.out.println("trait4/5");
							this.vue.afficherBienPlacé(this.combinaisonActuelle, nbBienPlacé);
							this.vue.afficherMalPlacé(this.combinaisonActuelle, nbMalPlacé);
							this.etat = Etat.DEBUT_COMBINAISON;
							this.combinaisonActuelle++;
							this.vue.desactiveCombinaison(this.combinaisonActuelle-1);
							this.vue.activerCombinaison(this.combinaisonActuelle);
							break;
						}
						if(nbEssaieRestant(this.combinaisonActuelle) == 0
						|| this.modele.estCorrect(valeurs)) {
							// trait4, trait6
							System.out.println("trait4/6");
							this.vue.afficherBienPlacé(this.combinaisonActuelle, nbBienPlacé);
							this.vue.afficherMalPlacé(this.combinaisonActuelle, nbMalPlacé);
							this.vue.afficherCombinaisonCouleur(this.modele.getCombinaison());
							this.fin = true;
							break;
						}
					}
					break;
				}
			}
			break;
		case DEBUT_COMBINAISON:
			if(comp instanceof JButton) {
				// clic BtCouleur / trait2
				if(this.vue.appartientPalette((JButton) comp)) {
					this.etat = Etat.CHOIX_COULEUR;
					this.couleurSelect = comp.getBackground();
				}
			}
			
			break;
		}
		
		System.out.println("Etat actuel : "+this.etat);
	}

	
	

}
