package net.p32.tp.mastermind.controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import net.p32.tp.mastermind.FenetreMastermind;

public class ControleurSauvegarder extends ControleurFenetre{
	
	public ControleurSauvegarder(FenetreMastermind instance) {
		super(instance);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		int res = JOptionPane.showConfirmDialog(null, "Voulez vous sauvegarder");
		switch(res) {
		case JOptionPane.NO_OPTION:
			break;
		case JOptionPane.YES_OPTION:
			String nomFichier = getInstance().dateActuelle()+getInstance().getFileExtension();
			getInstance().sauvegarderVueMasterMind(nomFichier);
			JOptionPane.showMessageDialog(null, "Fichier sauvegardé vers "+nomFichier);
			break;
		case JOptionPane.CANCEL_OPTION:
			break;
		}
		
	}

}
