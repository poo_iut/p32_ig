package net.p32.tp.couleur;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowListener;

import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

/**
 * controleur a l'ecoute des changements de focus sur les champs de texte
 * contenant les valeurs des nuances d'une couleur
 */
public class ControleurCouleur implements CaretListener {
    /**
     * vue associee au controleur
     */
    private VueCouleur vue;

    /**
     * cree un controleur en lui associant sa vue
     * 
     * @param vue
     *            : vue associee au controleur
     */
    public ControleurCouleur(VueCouleur vue) {
    	super();
        this.vue = vue;
    }

    /**
     * si on perd le focus sur un champ de texte alors recalculer la nouvelle
     * valeur RVB de la couleur en fonction des trois nuances RVB et afficher
     * cette couleur dans le canevas cCouleur de la vue
     * 
     * @param e
     *            evenement de focus
     */
    public void focusLost(FocusEvent e) {
        update();
    }

    public void update() {
    	Color c;
        // convertir le contenu du premier champ de texte
        // en un entier pour obtenir la nuance rouge
        //if(this.vue.getTRouge().getText()
        int v1 = parseColor(this.vue.getTRouge().getText());
        // convertir le contenu du deuxieme champ de texte
        // en un entier pour obtenir la nuance verte
        int v2 = parseColor(this.vue.getTVert().getText());
        // convertir le contenu du troisieme champ de texte
        // en un entier pour obtenir la nuance bleue
        int v3 = parseColor(this.vue.getTBleu().getText());
        // creer une couleur a partir des 3 nuances obtenues
        c = new Color(v1, v2, v3);
        // changer la couleur de fond du canevas couleurRVB
        // a partir de la nouvelle couleur obtenue
        this.vue.setCouleurCanvas(c);
    }
    
    public int parseColor(String text) {
    	if(text == null || text.length() == 0 || text.equals("")) {
    		return 0;
    	}
    	return Integer.parseInt(text);
    }
    
	@Override
	public void caretUpdate(CaretEvent arg0) {
		update();
	}
}
