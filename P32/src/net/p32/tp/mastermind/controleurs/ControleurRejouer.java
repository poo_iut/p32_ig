package net.p32.tp.mastermind.controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import net.p32.tp.mastermind.FenetreMastermind;

public class ControleurRejouer extends ControleurFenetre {

	public ControleurRejouer(FenetreMastermind fenetreMastermind) {
		super(fenetreMastermind);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int res = JOptionPane.showConfirmDialog(null, "Voulez vous rejouer ?");
		switch(res) {
		case JOptionPane.NO_OPTION:
			break;
		case JOptionPane.YES_OPTION:
			getInstance().creerNouvelleVue();
			break;
		case JOptionPane.CANCEL_OPTION:
			break;
		}
	}

}
