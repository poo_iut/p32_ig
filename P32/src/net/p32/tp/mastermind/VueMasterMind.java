package net.p32.tp.mastermind;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Arrays;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.p32.tp.mastermind.controleurs.ControleurMastermind;

public class VueMasterMind extends JPanel{

	public static final Color[] COLORS = {
			Color.BLUE,
			Color.GREEN,
			Color.YELLOW,
			Color.RED,
			Color.PINK,
			Color.CYAN,
			Color.BLACK,
			Color.MAGENTA,
			Color.ORANGE
	};
	
	public static Color chiffreEnCouleur(int index) {
		return COLORS[index];
	}
	
	public static int couleurEnChiffre(Color couleur) {
		for (int i = 0; i < COLORS.length; i++) {
			if(COLORS[i] == couleur)return i;
		}
		return -1;
	}
	
	private JTextField[] bienPlace;
	private JTextField[] malPlace;
	private JTextField[] validation;
	private JButton[][] propositions;
	private JButton[] palette;
	
	private int nbCouleur;
	private int nbChoix;
	private int nbTours;
	
	
	public VueMasterMind(int nbChoix, int nbTour, int nbCouleur) {
		if(nbCouleur > COLORS.length) {
			throw new IllegalArgumentException("Le nombre de couleur dépasse les couleurs");
		}
		setLayout(new BorderLayout());

		ControleurMastermind controleur = new ControleurMastermind(this);
		
		this.nbCouleur = nbCouleur;
		this.nbChoix = nbChoix;
		this.nbTours = nbTour;

		this.bienPlace = new JTextField[nbTour];
		this.malPlace = new JTextField[nbTour];
		this.validation = new JTextField[nbChoix];
		this.propositions = new JButton[nbTour][nbChoix];
		this.palette = new JButton[nbCouleur];
		
		
		// Le header
		JPanel topCouleur = new JPanel(new FlowLayout());
		JLabel labelCouleur = new JLabel("couleur");
		JPanel listeCouleur = new JPanel(new GridLayout(1, nbCouleur));
		for(int i = 0 ; i < nbCouleur; i++) {
			JButton jButton = new JButton();
			jButton.addActionListener(controleur);
			jButton.setBackground(COLORS[i]);
			listeCouleur.add(jButton);
			this.palette[i] = jButton;
		}
		topCouleur.add(labelCouleur);
		topCouleur.add(listeCouleur);

		JPanel centreChoix = new JPanel(new GridLayout(nbTour,2));
		for(int i = 0; i < nbTour; i++) {
			JPanel choixCouleur = new JPanel(new GridLayout(1,nbChoix));
			for(int j = 0; j < nbChoix; j++) {
				JButton jButton = new JButton();
				jButton.setEnabled(i == 0);
				jButton.addActionListener(controleur);
				choixCouleur.add(jButton);
				this.propositions[i][j] = jButton;
			}
			
			JPanel indicateurs = new JPanel(new GridLayout(2,2));

			JLabel bienPlaceLabel = new JLabel("Bien Placé");
			JLabel malPlaceLabel = new JLabel("Mal Placé");
			
			JTextField nbBienPlace = new JTextField();
			nbBienPlace.setEditable(false);
			this.bienPlace[i] = nbBienPlace;
			JTextField nbMalPlace = new JTextField();
			nbMalPlace.setEditable(false);
			this.malPlace[i] = nbMalPlace;

			indicateurs.add(bienPlaceLabel);
			indicateurs.add(malPlaceLabel);
			indicateurs.add(nbBienPlace);
			indicateurs.add(nbMalPlace);
			
			centreChoix.add(choixCouleur);
			centreChoix.add(indicateurs);
		}
		
		JPanel validation = new JPanel(new GridLayout(1, 2));
		JPanel validationCouleur = new JPanel(new GridLayout(1, nbChoix));
		for(int i = 0; i < nbChoix; i++) {
			JTextField couleur = new JTextField();
			couleur.setEditable(false);
			validationCouleur.add(couleur);
			this.validation[i] = couleur;
		}
		
		JButton valider = new JButton("Valider");
		valider.addActionListener(controleur);
		validation.add(validationCouleur);
		validation.add(valider);
		
		this.add(topCouleur, BorderLayout.NORTH);
		this.add(centreChoix, BorderLayout.CENTER);
		this.add(validation, BorderLayout.SOUTH);
		controleur.init();

	}
	
	public void activerCombinaison(int i) {
		System.out.println("Couche "+i+" activée");
		for(int y = 0; y < this.propositions[i].length; y++) {
			this.propositions[i][y].setEnabled(true);
		}
	}
	
	public void afficherBienPlacé(int i, int valeur) {
		this.bienPlace[i].setText(String.valueOf(valeur));
	}
	
	public void afficherMalPlacé(int i, int valeur) {
		this.malPlace[i].setText(String.valueOf(valeur));
	}
	
	public void afficherCombinaisonCouleur(int[] tabCouleurs) {
		for (int i = 0; i < tabCouleurs.length; i++) {
			this.validation[i].setBackground(chiffreEnCouleur(tabCouleurs[i]));
		}
	}
	
	public boolean appartientCombinaison(JButton boutton, int i) {
		for (int j = 0; j < this.propositions[i].length; j++) {
			if(this.propositions[i][j] == boutton)return true;
		}
		return false;
	}
	
	public boolean appartientPalette(JButton button) {
		for(JButton palette : this.palette) {
			if(palette == button)return true;
		}
		return false;
	}
	
	public boolean combinaisonComplete(int i) {
		for (int j = 0; j < propositions[i].length; j++) {
			if(couleurEnChiffre(this.propositions[i][j].getBackground()) == -1) {
				return false;
			}
		}
		return true;
	}
	
	public int[] combinaisonEnEntiers(int i) {
		int[] combi = new int[this.nbChoix];
		for (int j = 0; j < this.propositions[i].length; j++) {
			combi[j] = couleurEnChiffre(this.propositions[i][j].getBackground());
		}
		return combi;
	}
	
	public void desactiveCombinaison(int i) {
		for (JButton boutton : this.propositions[i]) {
			boutton.setEnabled(false);
		}
	}

	public int getNbChoix() {
		return nbChoix;
	}
	
	public int getNbCouleur() {
		return nbCouleur;
	}
	
	public int getNbTours() {
		return nbTours;
	}
}
