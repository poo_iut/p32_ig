package net.p32.tp.chessquito;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;

import chessquito.CaseVideException;
import chessquito.CouleurPieceException;
import chessquito.DeplacementInvalideException;
import chessquito.InterfaceChessquito;
import chessquito.PartieChessquito;
import chessquito.PartieNonInitialiseeException;
import chessquito.PartieTermineeException;
import chessquito.PositionInvalideException;
import net.p32.tp.chessquito.ihm.CaseIHM;
import net.p32.tp.chessquito.ihm.PieceIHM;
import net.p32.tp.chessquito.ihm.VueIHM;

public class ControleurChessquito implements ActionListener{

	private enum Etat{
		CHOIX_PIECE_BLANC,
		PLACE_PIECE_BLANC,
		CHOIX_PIECE_NOIR,
		PLACE_PIECE_NOIR,
		FIN;
	}
	
	private CaseIHM selectPiece;
	private Etat etat;
	private VueIHM vue;
	private InterfaceChessquito modele;

	public ControleurChessquito(VueIHM vue) {
		this.vue = vue;
		this.modele = new PartieChessquito();
		this.modele.initialiser();
		try {
			this.modele.remplirEchiquier();
		} catch (PartieNonInitialiseeException e) {
			e.printStackTrace();
		}
		this.etat = Etat.CHOIX_PIECE_BLANC;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		System.out.println("Etat actuel : "+this.etat);
		if(this.etat == Etat.FIN)return;
		CaseIHM caze = (CaseIHM) event.getSource();
		switch (this.etat) {
		case CHOIX_PIECE_BLANC:
			if(estPiece(caze)) {
				PieceIHM piece = (PieceIHM) caze.getIcon();
				if(piece.getCouleurPiece().equals("Blanc")) {
					System.out.println("La piece selectionée est : "+piece);
					this.selectPiece = caze;
					this.etat = Etat.PLACE_PIECE_BLANC;
				}
			}
			break;
		case PLACE_PIECE_BLANC:
			if(estCaseVide(caze)) {
				try {
					
					this.modele.jouer(this.selectPiece.getLigne(), this.selectPiece.getColonne(),
									  caze.getLigne(), caze.getColonne());

					this.rafraichir();
					this.selectPiece = null;
					this.etat = Etat.CHOIX_PIECE_NOIR;
					if(this.modele.estJoueurBlancGagnant()) {
						this.etat = Etat.FIN;
						afficherMessage("Les blancs ont gagné !");
						break;
					}
					if(this.modele.estPartieNulle()) {
						this.etat = Etat.FIN;
						afficherMessage("Partie nulle");
						break;
					}
				} catch (PartieNonInitialiseeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PartieTermineeException e) {
					e.printStackTrace();
				} catch (PositionInvalideException e) {
					e.printStackTrace();
				} catch (CouleurPieceException e) {
					e.printStackTrace();
				} catch (CaseVideException e) {
					e.printStackTrace();
				} catch (DeplacementInvalideException e) {
					afficherMessage("Déplacement incorrect, veuillez selectionner la piéce");
					this.etat = Etat.CHOIX_PIECE_BLANC;
				}
			}
			break;
		case CHOIX_PIECE_NOIR:
			if(estPiece(caze)) {
				PieceIHM piece = (PieceIHM) caze.getIcon();
				if(piece.getCouleurPiece().equals("Noir")) {
					this.selectPiece = caze;
					this.etat = Etat.PLACE_PIECE_NOIR;
				}
			}
			break;
		case PLACE_PIECE_NOIR:
			if(estCaseVide(caze)) {
				try {
					this.modele.jouer(this.selectPiece.getLigne(), this.selectPiece.getColonne(),
							  caze.getLigne(), caze.getColonne());

					this.rafraichir();
					this.selectPiece = null;
					this.etat = Etat.CHOIX_PIECE_BLANC;
					if(this.modele.estJoueurNoirGagnant()) {
						this.etat = Etat.FIN;
						afficherMessage("Les noirs ont gagné !");
						break;
					}
					if(this.modele.estPartieNulle()) {
						this.etat = Etat.FIN;
						afficherMessage("Partie nulle");
						break;
					}
				} catch (PartieNonInitialiseeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PartieTermineeException e) {
					e.printStackTrace();
				} catch (PositionInvalideException e) {
					e.printStackTrace();
				} catch (CouleurPieceException e) {
					e.printStackTrace();
				} catch (CaseVideException e) {
					e.printStackTrace();
				} catch (DeplacementInvalideException e) {
					afficherMessage("Déplacement incorrect, veuillez selectionner la piéce");
					this.etat = Etat.CHOIX_PIECE_NOIR;
				}
			}
			break;
		case FIN:
			break;
		
		}
		System.out.println("Etat a la fin : "+this.etat);
	}
	
	private void afficherMessage(final String message) {
		this.vue.afficherMessage(message);
		new Thread(() -> {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			if(this.vue.getMessages().getText().equals(message)) {
				this.vue.afficherMessage("");
			}
		});
	}
	
	private boolean estCaseVide(CaseIHM caze) {
		return caze.getIcon() == null;
	}
	
	private boolean estPiece(CaseIHM caze) {
		return caze.getIcon() != null && caze.getIcon() instanceof PieceIHM;
	}
	
	public void rafraichir() {
		try {
			// pour chaque case du plateau du modele
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					// verifier s'il existe une piece sur cette case
					String nom = this.modele.getNomPiece(i, j);
					if (nom != null) {
						// si c'est le cas positionner la piece sur le plateau
						// graphique
						String couleur = this.modele.getCouleurPiece(i, j);
						vue.positionnerPiece(new PieceIHM(couleur, nom, "Fixe"), i, j);
					} else {
						vue.positionnerPiece(null, i, j);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
